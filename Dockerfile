# This Dockerfile is a simple way to build a Go application and move the built application into a container.
FROM golang:latest as builder

LABEL maintainer="Gregory Ledray <greg@polyapp.tech>"

WORKDIR /go/src/app
ADD . /go/src/app

RUN go get -d -v ./...

RUN go build -o /go/bin/app ./main.go

# https://github.com/GoogleContainerTools/distroless
FROM gcr.io/distroless/base
COPY --from=builder /go/bin/app /
EXPOSE 443
CMD ["/app"]
