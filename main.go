package main

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"google.golang.org/api/iterator"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"golang.org/x/crypto/acme/autocert"
)

var (
	projectID        = ""
	autoCertDirCache = "~/autocert-cache"
	bucketName       = ""
	email            = ""
	mailgunSecret    = ""
)

func main() {
	initialize()
	e := echo.New()
	e.Pre(middleware.HTTPSRedirect())
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())
	e.Use(middleware.SecureWithConfig(middleware.SecureConfig{
		Skipper:               middleware.DefaultSkipper,
		XSSProtection:         "1; mode=block",
		XFrameOptions:         "SAMEORIGIN",
		HSTSMaxAge:            86400,
		HSTSExcludeSubdomains: true,
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST"},
		AllowCredentials: true,
		MaxAge:           120,
	}))

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	e.AutoTLSManager.Cache = autocert.DirCache(autoCertDirCache)
	// e.AutoTLSManager.HostPolicy = autocert.HostWhitelist(domain)
	e.GET("/*", GETHandler)
	e.POST("/*", POSTHandler)
	e.Logger.Fatal(e.StartAutoTLS(":" + port))
}

func initialize() {
	if bucketName == "" {
		bucketName = getRandString()
	}
	if email == "" {
		email = os.Getenv("ACCOUNT_EMAIL")
	}
	if mailgunSecret == "" {
		mailgunSecret = os.Getenv("MAILGUN_SECRET")
	}
	projectID = os.Getenv("GOOGLE_CLOUD_PROJECT")
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		panic(err)
	}
	defer client.Close()
	gcpBucket := client.Bucket(bucketName)
	_, err = gcpBucket.Attrs(ctx)
	if err != nil && err == storage.ErrBucketNotExist {
		// create the gcpBucket
		storageClassAndLocation := &storage.BucketAttrs{
			Location:     "us-central1",
			StorageClass: "REGIONAL",
		}
		err = gcpBucket.Create(ctx, projectID, storageClassAndLocation)
		if err != nil {
			panic(fmt.Errorf("Bucket(%q).Create: %v", gcpBucket, err))
		}
		// If we get here the bucketName was created successfully
	} else if err != nil {
		panic(err)
	}
}

// POSTHandler processes form submissions by sending them as an email.
func POSTHandler(e echo.Context) error {
	b, err := ioutil.ReadAll(e.Request().Body)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Could not read body")
	}
	// Warning: there is no check to verify that the POST request is being submitted by a person such as with Recaptcha.
	err = postSendEmail(b)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return nil
}

// GETHandler returns files from Google Cloud Storage to the user.
func GETHandler(e echo.Context) error {
	domain := e.Request().Host
	pathInGoogleCloudStorageBucket := domain + e.Request().URL.EscapedPath()
	if pathInGoogleCloudStorageBucket == domain || pathInGoogleCloudStorageBucket == domain + "/" {
		pathInGoogleCloudStorageBucket = domain + "/index.html"
	}

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient: %w", err)
	}
	defer client.Close()
	rc, err := client.Bucket(bucketName).Object(pathInGoogleCloudStorageBucket).NewReader(ctx)
	if err != nil {
		if err == storage.ErrObjectNotExist {
			log.Println(pathInGoogleCloudStorageBucket + " object not found: ")
			objs := client.Bucket(bucketName).Objects(ctx, nil)
			for i := 0; i < 30; i++ {
				o, err := objs.Next()
				if err != nil && err == iterator.Done {
					break
				}
				if o != nil {
					log.Println(o.Name)
				}
			}
			return echo.NewHTTPError(404, "Page Not Found")
		}
		return fmt.Errorf("Object(%q).NewReader: %v", pathInGoogleCloudStorageBucket, err)
	}
	_, err = io.Copy(e.Response().Writer, rc)
	if err != nil {
		return fmt.Errorf("io.Copy: %w", err)
	}
	err = rc.Close()
	if err != nil {
		return fmt.Errorf("rc.Close: %w", err)
	}
	e.Response().WriteHeader(http.StatusOK)
	// TODO set content-type header?
	return nil
}

func postSendEmail(body []byte) error {
	return nil
}

func getRandString() string {
	letters := []rune("abcdefghijklmnopqrstuvwxyz")
	b := make([]rune, 30)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
