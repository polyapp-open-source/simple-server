# Simple Server
This simple server handles requests to a secure website. If the request is to GET the website's contents, it replies with content from a Google Cloud Storage bucket. If the request is to POST a form, it sends that form via email.

# Get Started
* Go here to open Google Cloud's Console: https://console.cloud.google.com
* Activate Cloud Shell. Cloud Shell is an icon in the upper right hand corner that looks like this: ![Cloud Shell](https://miro.medium.com/max/422/1*21Rf-OKizee9oL0jKlPBzA.png)

After "provisioning" (which takes a while), you will see a black terminal taking up part of your screen. In the terminal will be some text like, "Welcome to Cloud Shell!".
* Copy this repository into your Cloud Shell, navigate to it, and run the installation script. To do that copy (Ctrl + C) and paste (Ctrl + V) the following into Cloud Shell and then press enter: 

rm -rf simple-server || true && git clone https://gitlab.com/polyapp-open-source/simple-server.git && cd simple-server && ./install.sh $GOOGLE_CLOUD_PROJECT && cd ..

* You MAY see a prompt, "Authorize Cloud Shell". Please click "Authorize".
* You SHOULD start to see a bunch of text appear in the Cloud Shell. The text represents the output from the script; it's expected.
* The script will be done when the cloudshell prompt appears. If it was successful, the line above that will say something like, "Installation finished successfully. Please point your domain at the public IP address shown below."

# Updating
* To update, re-do the steps in "Get Started". Everything is written to allow you to repeatedly update.

# License
This project uses the MIT license.
