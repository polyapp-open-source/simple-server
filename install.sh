# How to run this program: ./install.sh $GOOGLE_CLOUD_PROJECT
#
# This install.sh script finds or creates a public IP address, builds the Dockerfile in the repo, checks for an existing
# deployment, and if a deployment exists already it updates that deployment. If no deployment exists it creates a new one.
# The container should start automatically as part of deployment. For more information about what happens next, consult the "Dockerfile".
#
# Input 1: The Google Cloud Project ID which you are using to host your website.

MAILGUN_SECRET=""
ACCOUNT_EMAIL=$(gcloud auth list --format="value(account)")
PUBLIC_IP=$(gcloud compute addresses list --filter "NAME:website-ip" --format="value(address)")

if [ -z "$PUBLIC_IP" ]
then
  # PUBLIC_IP not found. We must create a new IP address and assign it to PUBLIC_IP
  gcloud compute addresses create website-ip --region=us-central1
  PUBLIC_IP=$(gcloud compute addresses list --filter "NAME:website-ip" --format="value(address)")
fi

gcloud builds submit --tag gcr.io/$1/website-1

gcloud compute instances describe --quiet --project $1 --zone us-central1-b website

if [ $? -gt 0 ]
then
  gcloud compute instances create-with-container website --quiet --project $1 --zone us-central1-b --container-image gcr.io/$1/website-1 --container-env GOOGLE_CLOUD_PROJECT=$1,PORT=443,ACCOUNT_EMAIL=$ACCOUNT_EMAIL --address $PUBLIC_IP --scopes=cloud-platform --tags=http-server,https-server
  # Google Cloud Platform Firewall setup
  gcloud compute --project $1 firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server
  gcloud compute --project $1 firewall-rules create default-allow-https --direction=INGRESS --priority=1001 --network=default --action=ALLOW --rules=tcp:443 --source-ranges=0.0.0.0/0 --target-tags=https-server
else
  gcloud compute instances update-container website --quiet --project $1 --zone us-central1-b --container-image gcr.io/$1/website-1 --container-env GOOGLE_CLOUD_PROJECT=$1,PORT=443,ACCOUNT_EMAIL=$ACCOUNT_EMAIL
fi

echo "Installation finished successfully. Please point your domain at the public IP address shown below."
echo $PUBLIC_IP
