module gitlab.com/polyapp-open-source/simple-server

go 1.14

require (
	cloud.google.com/go/storage v1.12.0
	github.com/labstack/echo/v4 v4.1.17
	golang.org/x/crypto v0.0.0-20200930160638-afb6bcd081ae
)
